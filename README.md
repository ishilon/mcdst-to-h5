A cpp script that reads an HESS MCDST .root file and converts it to a .h5 file format. 
The script utilises the HDF5 variable length data type to convert the telescopes images.
Build with a makefile. 
A short example python script for reading the HDF5 files is included as well.

The script prompts the user for an input directory and converts all .root files in that directory.
Due to the HDF5 H5F class limitations, the .h5 files will be created in the same directory in which the executable is running.
The name of the output HDF5 is identical to the .root file's name (except the extension is .h5 and not .root)
The environment variables in the makefile should be configured. 
HDF5 libraries should be installed and linked on your machine, including the h5c++ compiler which should be included by default with the HDF5 package installation.
For more details see: https://support.hdfgroup.org/HDF5/Tutor/compile.html
