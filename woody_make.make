CPPFLAGS = -g -std=c++11 -Wall $(shell root-config --cflags) -I${HESSROOT}/include -I/apps/hdf5/1.8.17-intel17.0/include
LDFLAGS = $(shell root-config --ldflags) 
LDLIBS = $(shell root-config --glibs) -L${HESSROOT}/lib -lrootHSenv -lrootHSbase -lrootHSevent -lrootsashfile -lrootmontecarlo -lrootstash -lrootmuon -lrootreco
CXX = g++
#CXX = h5c++

HDF5FLAGS = -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_BSD_SOURCE -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -L/apps/hdf5/1.8.17-intel17.0/lib /apps/hdf5/1.8.17-intel17.0/lib/libhdf5_hl_cpp.so /apps/hdf5/1.8.17-intel17.0/lib/libhdf5_cpp.so /apps/hdf5/1.8.17-intel17.0/lib/libhdf5_hl.so /apps/hdf5/1.8.17-intel17.0/lib/libhdf5.so -Wl,-z,relro -lz -ldl -lm -Wl,-rpath -Wl,/apps/hdf5/1.8.17-intel17.0/lib

PROG=mcdst-export-v2.5
SRCS = $(PROG).cpp
#OBJS = $(PROG).o
OBJS = $(subst .cpp,.o,$(SRCS))

$(PROG): $(OBJS)
    $(CXX) $(HDF5FLAGS) $(LDFLAGS) $(LDLIBS) -o $(PROG) $(OBJS)

$(OBJS): $(SRCS)
    #rootcint -c $(PROG).h
    $(CXX) $(CPPFLAGS) -c $(SRCS)

clean:
    rm -f *.o

