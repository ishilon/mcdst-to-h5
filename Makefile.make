CUSTOM_INCLUDE = /sps/hess/users/lpnhe/jlenain/local/include
CUSTOM_LIB = /sps/hess/users/lpnhe/jlenain/local/lib

CPPFLAGS = -g -std=c++11 -Wall $(shell root-config --cflags) -I${HESSROOT}/include -I$(CUSTOM_INCLUDE)
LDFLAGS = $(shell root-config --ldflags) 
LDLIBS = $(shell root-config --glibs) -L${HESSROOT}/lib -lrootHSenv -lrootHSbase -lrootHSevent -lrootsashfile -lrootmontecarlo -lrootstash -lrootmuon -lrootreco -lrootparisreco -lrootparisrecoSimu -lrootsmash -lrootpointing
CXX = /opt/rh/devtoolset-3/root/usr/bin/g++
# CXX = g++
# CXX = h5c++

HDF5FLAGS = -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_BSD_SOURCE -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -L$(CUSTOM_LIB) $(CUSTOM_LIB)/libhdf5_hl.so $(CUSTOM_LIB)/libhdf5.so -Wl,-z,relro -lz -ldl -lm -Wl,-rpath -Wl,$(CUSTOM_LIB)

SRCS := $(wildcard *.cpp)
OBJS := $(patsubst %.cpp,%.o,$(SRCS))
PROGS := $(patsubst %.cpp,%,$(SRCS))

all: $(PROGS)

mcdst-export.o: mcdst-export.cpp
	$(CXX) $(CPPFLAGS) -c $<

mccalibratedrawdata-export.o: mccalibratedrawdata-export.cpp
	$(CXX) $(CPPFLAGS) -c $<

mcdst-export: mcdst-export.o
	$(CXX) $(HDF5FLAGS) $(LDFLAGS) $(LDLIBS) $< -o $@

mccalibratedrawdata-export: mccalibratedrawdata-export.o
	$(CXX) $(HDF5FLAGS) $(LDFLAGS) $(LDLIBS) $< -o $@

#$(PROGS): $(OBJS)
#	@echo "Building $@ from $<"
#	$(CXX) $(HDF5FLAGS) $(LDFLAGS) $(LDLIBS) $< -o $@
#
#$(OBJS): $(SRCS)
#	@echo "Building $@ from $<"
#	$(CXX) $(CPPFLAGS) -c $<

.PHONY: clean
clean:
	$(RM) $(PROGS) $(OBJS)
